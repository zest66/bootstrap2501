$(document).ready(function(){
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        items: 1,
        loop:true,
        nav:true,
        autoplayTimeout:4000,
        autoplay:true,
        smartSpeed:1500,
        center:true,
        dots:true,
        mouseDrag:false,
        navText: ["&lt;", "&gt;"],
        responsive:{
          0: {
            touchDrag:true,
            autoplayTimeout:3000,
            dots:false,
            nav:false
          },
          768: {
              dots:false,
              touchDrag:true
          },
          992: {
              dots:true,
              nav:true
          }
        }
    })
});